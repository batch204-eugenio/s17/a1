console.log("Hello, 204!");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
//first function here:
// let fullnamePrompt = prompt("Enter youf full name.");
// console.log(fullnamePrompt);

function enterFullname() {
	let fullName = prompt("Enter your full name here.");
	console.log("Hi there " + fullName);
}
enterFullname();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function topFiveBands() {
	let band1 = "1. Maroon 5";
	let band2 = "2. Red Hot chili Peppers";
	let band3 = "3. Linkin Park";
	let band4 = "4. Paramore";
	let band5 = "5. Hoobastank";
	let bands =  band1 +"\n\n"+  band2 +"\n\n"+ band3 +"\n\n"+ band4 +"\n\n"+ band5;
	console.log(bands);
}
topFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFiveMovies() {
		let movie1 = "1. The Changeling";
		let movie2 = "2. Unbreakable";
		let movie3 = "3. Bit Fish";
		let movie4 = "4. Forrest Gump";
		let movie5 = "5. Cast Away";	
		let movies = movie1 +"\n\n"+ "The Rottent Tomato Rating: 91%" +"\n\n"+ movie2 +"\n\n"+ "The Rottent Tomato Rating: 93%" +"\n\n"+ movie3 +"\n\n"+ "The Rottent Tomato Rating: 90%" 
		             +"\n\n"+ movie4 +"\n\n"+ "The Rottent Tomato Rating: 98%" +"\n\n"+ movie5 +"\n\n"+ "The Rottent Tomato Rating: 95%";
		console.log(movies);
	}
	topFiveMovies();

	// other ways to solve
	// function topFiveMovies() {
	// 	let movie1 = "1. The Changeling";
	// 	let movie2 = "2. Unbreakable";
	// 	let movie3 = "3. Bit Fish";
	// 	let movie4 = "4. Forrest Gump";
	// 	let movie5 = "5. Cast Away";	
		
	// 	console.log(movie1 +"\n\n"+ "The Rottent Tomato Rating: 98%" +"\n\n"+ movie2 +"\n\n"+ "The Rottent Tomato Rating: 98%" +"\n\n"+ movie3 +"\n\n"+ "The Rottent Tomato Rating: 98%"
	// 		+"\n\n"+ movie4 +"\n\n"+ "The Rottent Tomato Rating: 98%" +"\n\n"+ movie5 +"\n\n"+ "The Rottent Tomato Rating: 98%");
	// }
	// topFiveMovies();

	// other ways to solve
	// function topFiveMovies() {
	// 	let movie1 = "1. The Changeling";
	// 	let movie2 = "2. Unbreakable";
	// 	let movie3 = "3. Bit Fish";
	// 	let movie4 = "4. Forrest Gump";
	// 	let movie5 = "5. Cast Away";	
		
	// 	console.log(movie1);
	// 	console.log("The Rotten Tomato Rating: 98%");
	// 	console.log(movie2);
	// 	console.log("The Rotten Tomato Rating: 98%");
	// 	console.log(movie3);
	// 	console.log("The Rotten Tomato Rating: 98%");
	// 	console.log(movie4);
	// 	console.log("The Rotten Tomato Rating: 98%");
	// 	console.log(movie5);
	// 	console.log("The Rotten Tomato Rating: 98%");
	// }
	// topFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends() = function printUsers(){
// 	alert("Hi! Please add the names of your friends.");
// 	let friend1 = alert("Enter your first friend's name:"); 
// 	let friend2 = prom("Enter your second friend's name:"); 
// 	let friend3 = prompt("Enter your third friend's name:");

// 	console.log("You are friends with:")
// 	console.log(friend1); 
// 	console.log(friend2); 
// 	console.log(friends); 
// };


// console.log(friend1);
// console.log(friend2);



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();